# JavaScript - vue-trivia-game

Project was built using nodejs, Vue framework and JavaScript.

This is a web application to play a trivia quiz game.
You can chose which category of questions you would like to receive.
How many questions you would like(5, 10, 15).
And lastly, the difficulty.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

You can start by downloading the project in a .zip file, or cloning the project using the terminal.

### Running the project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

## Maintainers 

[Robin Burø (@Robbur)](https://gitlab.com/Robbur)

## License

---
Copyright 2020, Robin Burø ([@Robbur](https://gitlab.com/Robbur))