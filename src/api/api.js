const BASE_URL = `https://opentdb.com/api.php?`;
const CATEGORY_URL = 'https://opentdb.com/api_category.php';

export const getTriviaQuestions = (amount, category, difficulty) => {
    return fetch( buildUrl(amount, category, difficulty) )
            .then(res => res.json())
            .then(res => res.results);
}

export const getTriviaCategories = () => {
    return fetch(CATEGORY_URL)
        .then(response => response.json())
        .then(response => response.trivia_categories);
}

const buildUrl = (a, c, d) => {
    return `${BASE_URL}amount=${a}&category=${c}&difficulty=${String(d).toLowerCase()}`;
}