export const setCategory = (selectedCategory) => {
  localStorage.setItem("category", selectedCategory);
};

export const getCategory = () => {
  return localStorage.getItem("category");
};

export const setAmount = (selectedAmount) => {
  localStorage.setItem("amount", selectedAmount);
};

export const getAmount = () => {
  return localStorage.getItem("amount");
};

export const setDifficulty = (selectedDifficulty) => {
  localStorage.setItem("difficulty", selectedDifficulty);
};

export const getDifficulty = () => {
  return localStorage.getItem("difficulty");
};

export const setScore = (totalScore) => {
  localStorage.setItem("score", totalScore);
};

export const getScore = () => {
  return localStorage.getItem("score");
};

export const setQuestions = (questions) => {
  localStorage.setItem("questions", JSON.stringify(questions));
};

export const getQuestions = () => {
  return JSON.parse(localStorage.getItem("questions"));
};

export const setMaxScore = (maxScore) => {
  localStorage.setItem("maxScore", maxScore);
};

export const getMaxScore = () => {
  return localStorage.getItem("maxScore");
};

export const setErrorMessage = (message) => {
  localStorage.setItem("message", message);
};

export const getErrorMessage = () => {
  return localStorage.getItem("message");
};
