import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';

import StartScreen from './components/StartScreen';
import TriviaScreen from './components/TriviaScreen';
import GameOverScreen from './components/GameOverScreen';

Vue.config.productionTip = false
Vue.use(VueRouter)

const routes = [
  { path: '/', component: StartScreen, props: true},
  { path: '/trivia', component: TriviaScreen},
  { path: '/gameOver', component: GameOverScreen},
];
const router = new VueRouter({routes});



new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
